package com.max.resouces;

import com.max.domains.Rating;
import com.max.domains.UserRating;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
    @RequestMapping("/ratingsdata")
public class RatingResource {

    @GetMapping("/{movieId}")
    public Rating getRating(@PathVariable String movieId){
        return new Rating(movieId,4);
    }
    @GetMapping("/users/{userId}")
    public UserRating getRatings(@PathVariable String userId){
        List<Rating> ratings = Arrays.asList(new Rating("123", 4),
                new Rating("124",3));
        UserRating userRating = new UserRating(ratings);
        userRating.setUserId(userId);
        return userRating;
    }
    @GetMapping("/health")
    public String health(){

        return "I am healthy";
    }
}
