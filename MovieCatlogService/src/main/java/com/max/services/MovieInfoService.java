package com.max.services;

import com.max.domains.Movie;
import com.max.domains.Rating;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MovieInfoService {
    @Autowired
    private RestTemplate restTemplate;
    @HystrixCommand(fallbackMethod = "getCatalogItemFallBackMethod")
    public Movie getCatlogItem(String movieId){
        return restTemplate.getForObject("http://MovieInfoService/movies/" + movieId, Movie.class);
    }

    public Movie getCatalogItemFallBackMethod(String movieId){
        return new Movie(movieId,"Not found");
    }
}
