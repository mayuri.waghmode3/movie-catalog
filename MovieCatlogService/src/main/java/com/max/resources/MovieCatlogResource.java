package com.max.resources;

import com.max.MovieCatlogApplication;
import com.max.domains.CatlogItem;
import com.max.domains.Movie;
import com.max.domains.Rating;
import com.max.domains.UserRating;
import com.max.services.MovieInfoService;
import com.max.services.UserRatingService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class MovieCatlogResource {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private MovieInfoService movieInfoService;
    @Autowired
    private UserRatingService userRatingService;

    @GetMapping("/catalog/{userId}")
    public List<CatlogItem> getCatalog(@PathVariable String userId) {

        UserRating userRating = userRatingService.getUserRatingInfo(userId);
        //UserRating userRating =restTemplate.getForObject("http://RatingDataService/ratingsdata/users/"+userId,UserRating.class);
        return userRating.getUserRating().stream()
                .map(rating -> {
                    Movie movie = movieInfoService.getCatlogItem(rating.getMovieId());
                    //Movie movie =restTemplate.getForObject("http://MovieInfoService/movies/" + rating.getMovieId(), Movie.class);
                    return new CatlogItem(movie.getName(),"action decp",rating.getRating());
                })
                .collect(Collectors.toList());
    }
}