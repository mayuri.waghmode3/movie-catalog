package com.max;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
@EnableHystrix
public class MovieCatlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovieCatlogApplication.class,args);
    }

    @Bean
    public WebClient.Builder getWebClientBuilder(){
         return WebClient.builder();
    }


    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate(){
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory() ;
        factory.setConnectTimeout(3000);
        return new RestTemplate(factory);
    }
}
