package com.max.domains;

public class CatlogItem {
    private String name;
    private String descp;
    private int rating;

    public CatlogItem(String name, String descp, int rating) {
        this.name = name;
        this.descp = descp;
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
