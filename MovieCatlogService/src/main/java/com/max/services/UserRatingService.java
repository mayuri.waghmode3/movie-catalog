package com.max.services;

import com.max.domains.Rating;
import com.max.domains.UserRating;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
@Service
public class UserRatingService {
    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "getUserRatingFallBackMethod")
    public UserRating getUserRatingInfo(String userId){
        return restTemplate.getForObject("http://RatingDataService/ratingsdata/users/"+userId,UserRating.class);
    }

    public UserRating getUserRatingFallBackMethod(String userId){
        UserRating userRating= new UserRating();
        userRating.setUserId(userId);
        userRating.setUserRating(Arrays.asList(new Rating( "",0)));
        return userRating;
    }
}
